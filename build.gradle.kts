plugins {
    id("java")
    id("org.jetbrains.intellij") version "1.3.0"
}

group = "com.burningmime.mimescript.language"
version = "0.1-SNAPSHOT"
repositories { mavenCentral() }
sourceSets["main"].java.srcDirs("src/main/gen")

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

intellij {
    updateSinceUntilBuild.set(false)
    pluginName.set("MimeScript Language")
    version.set("2021.2")
    type.set("RD")
}
