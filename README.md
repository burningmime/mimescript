This is a JetBrains Rider plugin that transpiles a custom visual novel language into C# that can be used
in my Unity project. The initial implementation was timeboxed to a week, which is why the code is a bit
messy; I've been cleaning it up over time. It's somewhere between "total prototype" and "prototype being
used to actually make an indie game". Which is a scary, scary place to be in terms of code quality ;-P.

This project probably won't be that useful to anyone but me, but I'm making it public in case I ever
want to make a tutorial about it or reference it on the Unity forums or something. It also doesn't contain
any copyright/external content, not much in the way of "trade secrets" that I want kept, and hopefully
nothing too offensive.

If you actually want to learn how to make a JetBrains plugin for a DSL, read here: https://plugins.jetbrains.com/docs/intellij/custom-language-support-tutorial.html
The only slightly interesting pieces that aren't covered in that tutorial are how to manage indentation-based
syntax in the lexer, and how to generate code whenever a file is saved (neither of which are super complex).

Note if you try to build it and it has some complaints about zzAtBOL : https://stackoverflow.com/questions/75840456/how-to-properly-resolve-cannot-find-symbol-after-run-jflex-generator-in-inte
THis is an IntelliJ bug; nothing I can do to solve it.