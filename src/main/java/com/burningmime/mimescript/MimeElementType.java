package com.burningmime.mimescript;

import com.burningmime.mimescript.MimeLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class MimeElementType extends IElementType {
    public MimeElementType(@NonNls @NotNull String debugName) {
        super(debugName, MimeLanguage.INSTANCE);
    }
}
