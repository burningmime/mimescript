package com.burningmime.mimescript;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

public class MimeFile extends PsiFileBase
{
    public MimeFile(@NotNull FileViewProvider viewProvider) { super(viewProvider, MimeLanguage.INSTANCE); }
    @NotNull @Override public FileType getFileType() { return MimeFileType.INSTANCE; }
    @Override public String toString() { return "MimeScript File"; }
}
