package com.burningmime.mimescript;

import com.intellij.lang.annotation.*;
import com.intellij.openapi.application.ReadAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public class MimeAnnotator extends ExternalAnnotator<MimeFile, List<MimeError>> {

    @Override
    public @Nullable MimeFile collectInformation(@NotNull PsiFile file, @NotNull Editor editor, boolean hasErrors) {
        return !hasErrors && file instanceof MimeFile ? (MimeFile) file : null;
    }

    @Override
    public @Nullable List<MimeError> doAnnotate(MimeFile file) {
        if(file == null)
            return Collections.emptyList();
        MimeErrorCollector err = new MimeErrorCollector(null);
        ReadAction.run(() -> MimeCodeGenerator.dryRun(file, err));
        return err.getErrors();
    }

    @Override
    public void apply(@NotNull PsiFile file, List<MimeError> annotationResult, @NotNull AnnotationHolder holder) {
        if(annotationResult != null) {
            for(MimeError err : annotationResult) {
                AnnotationBuilder ab = holder.newAnnotation(HighlightSeverity.ERROR, err.message);
                if(err.context != null) {
                    ab = ab.range(err.context);
                }
                ab.create();
            }
        }
    }
}
