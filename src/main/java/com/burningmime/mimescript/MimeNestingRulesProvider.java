package com.burningmime.mimescript;

import com.intellij.ide.projectView.ProjectViewNestingRulesProvider;
import org.jetbrains.annotations.NotNull;

public class MimeNestingRulesProvider implements ProjectViewNestingRulesProvider {
    @Override
    public void addFileNestingRules(@NotNull Consumer consumer) {
        consumer.addNestingRule(".mime", ".cs");
        consumer.addNestingRule(".mime", ".g.en-us.stringpack");
    }
}
