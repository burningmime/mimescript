package com.burningmime.mimescript;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.LiteralTextEscaper;
import com.intellij.psi.PsiLanguageInjectionHost;
import com.intellij.psi.impl.source.tree.LeafElement;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MimeCSharpInjectionHost extends ASTWrapperPsiElement implements PsiLanguageInjectionHost {
    public MimeCSharpInjectionHost(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public boolean isValidHost() {
        return true;
    }

    @Override
    public PsiLanguageInjectionHost updateText(@NotNull String text) {
        ASTNode valueNode = getNode().getFirstChildNode();
        assert valueNode instanceof LeafElement;
        ((LeafElement)valueNode).replaceWithText(text);
        return this;
    }

    @Override
    public @NotNull LiteralTextEscaper<? extends PsiLanguageInjectionHost> createLiteralTextEscaper() {
        return new LiteralTextEscaper<>(this) {
            private List<MimeCSharpInjectionRange> ranges;

            @Override
            public boolean decode(@NotNull TextRange rangeInsideHost, @NotNull StringBuilder outChars) {
                ranges = new ArrayList<>();
                MimeCSharpInjectionUtils.replaceTemplateParts(
                    rangeInsideHost.substring(myHost.getText()),
                    outChars,
                    (hs, he, ds, de) -> ranges.add(new MimeCSharpInjectionRange(hs, he, ds, de)));
                return true;
            }

            @Override
            public int getOffsetInHost(int offsetInDecoded, @NotNull TextRange rangeInsideHost) {
                if(ranges == null) {
                    return rangeInsideHost.getStartOffset() + offsetInDecoded;
                } else {
                    for(MimeCSharpInjectionRange r : ranges) {
                        if(offsetInDecoded >= r.decodedStart && offsetInDecoded < r.decodedEnd) {
                            return offsetInDecoded - r.decodedStart + r.hostStart;
                        }
                    }
                    return -1;
                }
            }

            @Override
            public boolean isOneLine() {
                return true;
            }

            class MimeCSharpInjectionRange {
                public final int hostStart;
                public final int hostEnd;
                public final int decodedStart;
                public final int decodedEnd;

                public MimeCSharpInjectionRange(int hostStart, int hostEnd, int decodedStart, int decodedEnd) {
                    this.hostStart = hostStart;
                    this.hostEnd = hostEnd;
                    this.decodedStart = decodedStart;
                    this.decodedEnd = decodedEnd;
                }

                @Override
                public String toString() {
                    return "SourceReplacementRange{[" + hostStart + "," + hostEnd + ") -> [" + decodedStart + "," + decodedEnd + ")}";
                }
            }
        };
    }
}
