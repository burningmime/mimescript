package com.burningmime.mimescript;

import com.burningmime.mimescript.psi.MimeCSharpLiteral;
import com.intellij.lang.injection.general.Injection;
import com.intellij.lang.injection.general.LanguageInjectionContributor;
import com.intellij.lang.injection.general.SimpleInjection;
import com.intellij.psi.PsiElement;
import com.jetbrains.rider.ideaInterop.fileTypes.csharp.CSharpLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class MimeCSharpInjectionContributor implements LanguageInjectionContributor {
    @Override
    public @Nullable Injection getInjection(@NotNull PsiElement context) {
        if(context instanceof MimeCSharpLiteral) {
            String prefix = "", suffix = ""; // TODO
            return new SimpleInjection(CSharpLanguage.INSTANCE, prefix, suffix, null);
        } else {
            return null;
        }
    }
}
