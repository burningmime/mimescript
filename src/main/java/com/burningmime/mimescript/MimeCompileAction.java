package com.burningmime.mimescript;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

public class MimeCompileAction extends AnAction {
    @Override
    public void update(@NotNull AnActionEvent evt) {
        boolean enabled = false;
        VirtualFile[] files = evt.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);
        if(files != null && files.length > 0) {
            for(VirtualFile file : files) {
                if(file.getFileType() == MimeFileType.INSTANCE) {
                    enabled = true;
                    break;
                }
            }
        }
        this.getTemplatePresentation().setEnabledAndVisible(enabled);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent evt) {
        VirtualFile[] files = evt.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);
        Project project = evt.getProject();
        if(files != null && project != null) {
            for(VirtualFile file : files) {
                if(file.getFileType() == MimeFileType.INSTANCE) {
                    MimeCompiler.compile(project, file, true);
                }
            }
        }
    }
}
