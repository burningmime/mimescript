package com.burningmime.mimescript;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public final class MimeError {
    public final @NotNull String message;
    public final @Nullable PsiElement context;

    public MimeError(@NotNull String message, @Nullable PsiElement context) {
        this.message = message;
        this.context = context;
    }
}
