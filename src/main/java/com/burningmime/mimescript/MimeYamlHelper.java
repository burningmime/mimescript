package com.burningmime.mimescript;

import org.jetbrains.annotations.NotNull;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;
import java.util.Set;

class MimeYamlHelper {

    @NotNull private static final Yaml yaml;

    public static @NotNull String toYaml(Object obj) {
        return yaml.dump(obj);
    }

    static
    {
        Representer yamlRepresenter = new Representer() {
            @Override
            protected MappingNode representJavaBean(Set<Property> properties, Object javaBean) {
                if(!classTags.containsKey(javaBean.getClass())) {
                    addClassTag(javaBean.getClass(), Tag.MAP);
                }
                return super.representJavaBean(properties, javaBean);
            }
        };
        yamlRepresenter.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        yaml = new Yaml(yamlRepresenter);
    }
}
