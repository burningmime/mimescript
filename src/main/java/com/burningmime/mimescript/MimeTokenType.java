package com.burningmime.mimescript;

import com.burningmime.mimescript.MimeLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class MimeTokenType extends IElementType {
    public MimeTokenType(@NonNls @NotNull String debugName) {
        super(debugName, MimeLanguage.INSTANCE);
    }
}
