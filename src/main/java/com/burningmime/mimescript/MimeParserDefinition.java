package com.burningmime.mimescript;

import com.burningmime.mimescript.psi.MimeLexer;
import com.burningmime.mimescript.psi.MimeParser;
import com.burningmime.mimescript.psi.Tok;
import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;
import com.intellij.psi.TokenType;

public class MimeParserDefinition implements ParserDefinition {

    private static final TokenSet WHITE_SPACES = TokenSet.create(TokenType.WHITE_SPACE);
    private static final TokenSet COMMENTS = TokenSet.create(Tok.T_COMMENT);
    private static final TokenSet STRING_LITERALS = TokenSet.create(Tok.T_TEXT);
    private static final IFileElementType FILE_NODE_TYPE = new IFileElementType(MimeLanguage.INSTANCE);

    @Override public @NotNull Lexer createLexer(Project project) { return getLexer(true); }
    @Override public PsiParser createParser(Project project) { return new MimeParser(); }
    @Override public IFileElementType getFileNodeType() { return FILE_NODE_TYPE; }
    @Override public @NotNull TokenSet getWhitespaceTokens() { return WHITE_SPACES; }
    @Override public @NotNull TokenSet getCommentTokens() { return COMMENTS; }
    @Override public @NotNull TokenSet getStringLiteralElements() { return STRING_LITERALS; }
    @Override public @NotNull PsiElement createElement(ASTNode node) { return Tok.Factory.createElement(node); }
    @Override public PsiFile createFile(FileViewProvider viewProvider) { return new MimeFile(viewProvider); }

    public static @NotNull Lexer getLexer(boolean emitIndentation) { return new FlexAdapter(new MimeLexer(null, emitIndentation)); }
}
