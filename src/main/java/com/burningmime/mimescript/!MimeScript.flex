package com.burningmime.mimescript.psi;

import java.util.Stack;
import java.lang.StringBuilder;
import com.intellij.lexer.FlexLexer;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;

%%

%public
%class MimeLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%ctorarg boolean emitIndentation

%x S_INDENT
%x S_EMIT
%x S_BEGIN
%x S_COMMAND
%x S_TEXT_BEGIN
%x S_TEXT_MOD
%x S_TEXT_MAIN
%x S_TEXT_SLASH
%x S_TEXT_NEWLINE
%x S_CSHARP

%{
    private static final int TAB_WIDTH = 4;
    private final boolean emitIndentation;
    private final Stack<Integer> indentLevels;
    private final Stack<IElementType> delayed;
    private int currentIndent;
    private int afterEmitState = S_BEGIN;

    private IElementType onReachedEof(boolean addLF) {
        if(emitIndentation) {
            // we set state to emit no matter what so we don't end up repeatedly having this method called
            afterEmitState = -1;
            yybegin(S_EMIT);

            // we need to outdent any indent levels we have (but bottom of stack is always 0)
            int outdentCount = indentLevels.size() - 1;
            if(outdentCount > 0) {
                for(int i = 0; i < outdentCount; ++i) {
                    delayed.add(Tok.T_OUTDENT);
                }
            }

            // we may want to emit a linefeed to finish off some statements since end of file should also be end of line
            if(addLF) {
                return Tok.T_LF;
            } else if(!delayed.isEmpty()) {
                return delayed.pop();
            }
        }
        return null;
    }
%}

%init{
    this.emitIndentation = emitIndentation;
    if(emitIndentation) {
        indentLevels = new Stack<>();
        delayed = new Stack<>();
        indentLevels.push(0);
    } else {
        indentLevels = null;
        delayed = null;
    }
%init}

C_COMMENT=#[^\n]*
C_WS=[\ \t]+
C_IDENTIFIER=[:jletter:][:jletterdigit:]*
C_SLUG=[A-Z][A-Z0-9]
C_ANY=[^]
C_NUMBER=\-?(([0-9]+(\.[0-9]+)?)|(\.[0-9]+))
// C_QUOTEDSTRING=\"[^\"]+\"

%%

// always start in the indent state
<YYINITIAL> {
    {C_ANY} { yypushback(1); yybegin(S_INDENT); }
}

// if there are any tokens waiting to be emitted (usually indent and outdent), they are emitted here
// all these tokens are "fake" tokens, that is they have no actual presence in the source code
<S_EMIT> {
    {C_ANY} {
          yypushback(1);
          if(delayed.isEmpty()) {
              yybegin(afterEmitState);
          } else {
              IElementType delayToken = delayed.pop();
              if(delayed.isEmpty()) {
                  yybegin(afterEmitState);
              }
              return delayToken;
          }
      }
    <<EOF>> { return delayed.isEmpty() ? null : delayed.pop(); }
}

// we count the number of indents we get and emit them as white space. but if the indentation is different from the
// previous line, we also need to emit indent/outdent tokens, which we do via the S_EMIT state
<S_INDENT> {
    \r { /* ignore */ }
    \n { currentIndent = 0; return TokenType.WHITE_SPACE; }
    {C_COMMENT} { return Tok.T_COMMENT; }
    " " { currentIndent++; }
    \t { currentIndent = (currentIndent + TAB_WIDTH) & ~(TAB_WIDTH-1); }
    {C_ANY} {
              yypushback(1);
              if(emitIndentation) {
                  int prevIndent = indentLevels.peek();
                  if(currentIndent > prevIndent) {
                      indentLevels.push(currentIndent);
                      delayed.push(Tok.T_INDENT);
                  } else if(currentIndent < prevIndent) {
                      // handle multiple levels of outdent
                      indentLevels.pop();
                      int outdentCount = 1;
                      while(currentIndent < indentLevels.peek()) {
                          ++outdentCount;
                          indentLevels.pop(); }

                      // handle a change in indentation level
                      // TODO what's the corrent semantics here? honestly feels like it should be an error, but see what python, etc do
                      if(currentIndent > indentLevels.peek()) {
                          indentLevels.push(currentIndent);
                          --outdentCount;
                      }

                      for(int i = 0; i < outdentCount; ++i) {
                            delayed.push(Tok.T_OUTDENT);
                      }
                  }
                  if(delayed.isEmpty()) {
                      yybegin(S_BEGIN);
                  } else {
                      afterEmitState = S_BEGIN;
                      yybegin(S_EMIT);
                  }
              } else {
                  yybegin(S_BEGIN);
              }

              if(currentIndent > 0) {
                  currentIndent = 0;
                  return TokenType.WHITE_SPACE;
              }
          }
    <<EOF>> { return onReachedEof(false); }
}

// beginning of line -- these are mutually exclusive so the parser knows right away what kind of line we're dealing with
<S_BEGIN> {
    \r { /* ignore */ }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    {C_COMMENT} { return Tok.T_COMMENT; }

    "char" { yybegin(S_COMMAND); return Tok.K_CHAR; }
    "mode" { yybegin(S_COMMAND); return Tok.K_MODE;  }
    "using" { yybegin(S_COMMAND); return Tok.K_USING; }
    "namespace" { yybegin(S_COMMAND); return Tok.K_NAMESPACE; }
    "sceneid" { yybegin(S_COMMAND); return Tok.K_SCENEID; }
    "if" { yybegin(S_CSHARP); return Tok.K_IF; }
    "elif" { yybegin(S_CSHARP); return Tok.K_ELIF; }
    "while" { yybegin(S_CSHARP); return Tok.K_WHILE; }
    "label" { yybegin(S_COMMAND); return Tok.K_LABEL; }
    "goto" { yybegin(S_COMMAND); return Tok.K_GOTO; }
    "wait" { yybegin(S_COMMAND); return Tok.K_WAIT; }
    "spawn" { yybegin(S_COMMAND); return Tok.K_SPAWN; }
    "despawn" { yybegin(S_COMMAND); return Tok.K_DESPAWN; }
    "turn" { yybegin(S_COMMAND); return Tok.K_TURN; }
    "pass" { yybegin(S_COMMAND); return Tok.K_PASS; }
    "end" { yybegin(S_COMMAND); return Tok.K_END; }
    "adjustpp" { yybegin(S_COMMAND); return Tok.K_ADJUSTPP; }
    "choice" { yybegin(S_COMMAND); return Tok.K_CHOICE; }
    "else" { yybegin(S_COMMAND); return Tok.K_ELSE; }
    "image" { yybegin(S_COMMAND); return Tok.K_IMAGE; }
    "imagehide" { yybegin(S_COMMAND); return Tok.K_IMAGEHIDE; }
    "clook" { yybegin(S_COMMAND); return Tok.K_CLOOK; }
    "cclear" { yybegin(S_COMMAND); return Tok.K_CCLEAR; }
    "bg" { yybegin(S_COMMAND); return Tok.K_BG; }
    "dsub" { yybegin(S_CSHARP); return Tok.K_SUB; }
    "setidle" { yybegin(S_COMMAND); return Tok.K_SETIDLE; }
    "walk" { yybegin(S_COMMAND); return Tok.K_WALK; }

    \> { yybegin(S_CSHARP); return Tok.T_GREATER; }
    \* { yybegin(S_TEXT_BEGIN); return Tok.T_STAR; }
    {C_SLUG} { yybegin(S_TEXT_BEGIN); return Tok.T_SLUG; }
    {C_ANY} { return TokenType.BAD_CHARACTER; /* note we shouldn't get any white space here; indent should pick up all of it */ }
    <<EOF>> { return onReachedEof(false); }
}

<S_TEXT_BEGIN> {
    \r { /* ignore */ }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    "<<" { yybegin(S_TEXT_MOD); return Tok.T_LANGLE; }
    {C_WS} { return TokenType.WHITE_SPACE; }
    {C_ANY} { yypushback(1); yybegin(S_TEXT_MAIN); }
    <<EOF>> { return onReachedEof(true); }
}

<S_TEXT_MOD> {
    \r { /* ignore */ }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    \, { return Tok.T_COMMA; }
    \= { return Tok.T_EQUALS; }
    ">>" { yybegin(S_TEXT_MAIN); return Tok.T_RANGLE; }
    "self" { return Tok.K_SELF; }
    "null" { return Tok.K_NULL; }
    {C_SLUG} { return Tok.T_SLUG; }
    {C_IDENTIFIER} { return Tok.T_ID; }
    // {C_QUOTEDSTRING} { return Tok.T_STRING; }
    {C_NUMBER} { return Tok.T_NUMBER; }
    {C_WS} { return TokenType.WHITE_SPACE; }
    {C_ANY} { return TokenType.BAD_CHARACTER; }
    <<EOF>> { return onReachedEof(true); }
}

// this is a pretty generic lexing state which just emits whatever it sees to the underlying parser
<S_COMMAND> {
    \r { /* ignore */ }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    "self" { return Tok.K_SELF; }
    "null" { return Tok.K_NULL; }
    "interactive" { return Tok.K_INTERACTIVE; }
    "static" { return Tok.K_STATIC; }
    "nodialogue" { return Tok.K_NODIALOGUE; }
    "wait" { return Tok.K_WAIT; }
    "spawn" { return Tok.K_SPAWN; }
    "despawn" { return Tok.K_DESPAWN; }
    "adjustpp" { return Tok.K_ADJUSTPP; }
    "clook" { return Tok.K_CLOOK; }
    "dsub" { yybegin(S_CSHARP); return Tok.K_SUB; }
    "all" { return Tok.K_ALL; }
    {C_SLUG} { return Tok.T_SLUG; }
    {C_IDENTIFIER} { return Tok.T_ID; }
    // {C_QUOTEDSTRING} { return Tok.T_STRING; }
    \. { return Tok.T_DOT; }
    {C_NUMBER} { return Tok.T_NUMBER; }
    \, { return Tok.T_COMMA; }
    \= { return Tok.T_EQUALS; }
    \( { return Tok.T_LPAREN; }
    \) { return Tok.T_RPAREN; }
    {C_COMMENT} { return Tok.T_COMMENT; }
    {C_WS} { return TokenType.WHITE_SPACE; }
    {C_ANY} { return TokenType.BAD_CHARACTER; }
    <<EOF>> { return onReachedEof(true); }
}

<S_TEXT_MAIN> {
    [^\n\\\#]+ { return Tok.T_TEXT; }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    \\ { yybegin(S_TEXT_SLASH); return Tok.T_BACKSLASH; }
    {C_COMMENT} { return Tok.T_COMMENT; }
    <<EOF>> { return onReachedEof(true); }
}

<S_TEXT_SLASH> {
    {C_WS} { return TokenType.WHITE_SPACE; }
    {C_COMMENT} { return Tok.T_COMMENT; }
    \n { yybegin(S_TEXT_NEWLINE); return Tok.T_LF; }
    [^] { return TokenType.BAD_CHARACTER; }
    <<EOF>> { return onReachedEof(true); }
}

<S_TEXT_NEWLINE> {
    {C_WS} { return TokenType.WHITE_SPACE; }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    \\ { yybegin(S_TEXT_SLASH); return Tok.T_BACKSLASH; }
    [^] { yypushback(1); yybegin(S_TEXT_MAIN); }
    <<EOF>> { return onReachedEof(false); }
}

// pick up everything until the end of line and emit it as a string
<S_CSHARP> {
    [^\n]+ { return Tok.T_CSHARP; }
    \n { yybegin(S_INDENT); return Tok.T_LF; }
    <<EOF>> { return onReachedEof(true); }
}
