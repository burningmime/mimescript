package com.burningmime.mimescript;

import com.intellij.openapi.application.ReadAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileDocumentManagerListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class MimeDocumentManagerListener implements FileDocumentManagerListener {
    private final Project project;

    public MimeDocumentManagerListener(Project project) {
        this.project = project;
    }

    @Override
    public void beforeDocumentSaving(@NotNull Document document) {
        VirtualFile virtualFile = getVirtualFile(document);
        if(virtualFile != null) {
            MimeCompiler.compile(project, virtualFile, false);
        }
    }

    private static @Nullable VirtualFile getVirtualFile(@NotNull Document document) {
        return ReadAction.compute(() -> {
            FileDocumentManager fdm = FileDocumentManager.getInstance();
            VirtualFile vf = fdm.getFile(document);
            return vf == null || vf.getFileType() != MimeFileType.INSTANCE ? null : vf;
        });
    }
}
