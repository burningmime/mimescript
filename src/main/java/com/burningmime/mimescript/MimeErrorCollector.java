package com.burningmime.mimescript;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class MimeErrorCollector {
    private final List<MimeError> errors = new ArrayList<>();
    @Nullable private final String filename;

    public MimeErrorCollector(@Nullable String filename) {
        this.filename = filename;
    }

    public void error(@NotNull String message, @Nullable PsiElement context) {
        if(filename != null) {
            message = "[" + filename + "] " + message;
        }
        errors.add(new MimeError(message, context));
    }

    public void catastrophe(@NotNull Exception ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        error(sw.toString(), null);
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public List<MimeError> getErrors() {
        return Collections.unmodifiableList(errors);
    }
}