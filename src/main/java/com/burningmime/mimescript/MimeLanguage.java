package com.burningmime.mimescript;

import com.intellij.lang.Language;

public class MimeLanguage extends Language
{
    public static final MimeLanguage INSTANCE = new MimeLanguage();
    private MimeLanguage() { super("MimeScript"); }
}
