package com.burningmime.mimescript;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class MimeCSharpInjectionUtils {

    public static void replaceTemplateParts(@NotNull String input, @NotNull StringBuilder output, @Nullable ISourceReplacementObserver sro) {
        for(int i = 0; i < input.length(); ++i) {
            char c = input.charAt(i);
            if(c == '$' && i < input.length() - 1) {
                int ss = i, ds = output.length();
                char n = input.charAt(i + 1);
                if(Character.isJavaIdentifierStart(n)) {
                    i++;
                    int start = i;
                    for(; i < input.length() && Character.isJavaIdentifierPart(input.charAt(i)); ++i) { /* pass */ }
                    String varName = input.substring(start, i);
                    output.append("vars[\"").append(varName).append("\"]");
                    int se = i, de = output.length();
                    if(i < input.length())
                        --i;
                    if(sro != null)
                        sro.replacedRange(ss, se, ds, de);
                } else {
                    output.append(c);
                }
            }
            else {
                output.append(c);
            }
        }
    }

    public interface ISourceReplacementObserver {
        void replacedRange(int hostStart, int hostEnd, int decodedStart, int decodedEnd);
    }
}
