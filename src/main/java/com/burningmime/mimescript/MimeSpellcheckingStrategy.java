package com.burningmime.mimescript;

import com.burningmime.mimescript.psi.MimeChoiceString;
import com.burningmime.mimescript.psi.MimeDialogueSegment;
import com.intellij.psi.PsiElement;
import com.intellij.spellchecker.tokenizer.SpellcheckingStrategy;
import com.intellij.spellchecker.tokenizer.Tokenizer;
import org.jetbrains.annotations.NotNull;

public class MimeSpellcheckingStrategy extends SpellcheckingStrategy {
    @SuppressWarnings("rawtypes") @NotNull @Override
    public Tokenizer getTokenizer(PsiElement element) {
        if(element instanceof MimeChoiceString || element instanceof MimeDialogueSegment) {
            return TEXT_TOKENIZER;
        }
        return super.getTokenizer(element);
    }
}
