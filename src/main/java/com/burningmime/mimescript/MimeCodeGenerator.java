package com.burningmime.mimescript;

import com.burningmime.mimescript.psi.*;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class MimeCodeGenerator extends MimeVisitor {

    //==================================================================================================================
    // PUBLIC API
    //==================================================================================================================

    public static @Nullable Result run(@NotNull MimeFile inFile, @NotNull MimeErrorCollector err) {
        return run(inFile, err, false);
    }

    public static void dryRun(@NotNull MimeFile inFile, @NotNull MimeErrorCollector err) {
        run(inFile, err, true);
    }

    public static final class Result {
        public final @NotNull String cSharp;
        public final @Nullable String stringPack;
        public Result(@NotNull String cSharp, @Nullable String stringPack) {
            this.cSharp = cSharp;
            this.stringPack = stringPack;
        }
    }

    //==================================================================================================================
    // FIELDS & CONSTRUCTOR
    //==================================================================================================================

    @Nullable private static String validDialogueModifiersCache;
    @Nullable private static String validCameraModifiersCache;
    @NotNull private final VirtualFile virtualFile;
    @NotNull private final MimeErrorCollector err;
    @NotNull private final IIndentPrinter p;
    @NotNull private final String className;
    @NotNull private final Map<String, CharRef> charRefs;
    @NotNull private final List<String> usingDeclarations;
    @NotNull private final List<String> sceneIds;
    @NotNull private final List<LocaleStringEntry> locStrings;
    @NotNull private ClassMode mode;
    @NotNull private String namespace;
    @Nullable private String backgroundToken;
    private int nextLocKeyNum;
    private boolean hasImage;

    private static @Nullable Result run(@NotNull MimeFile inFile, @NotNull MimeErrorCollector err, boolean isDryRun) {
        try {
            MimeCodeGenerator code = new MimeCodeGenerator(err, inFile.getVirtualFile(), isDryRun);
            inFile.accept(code);
            if(isDryRun || err.hasErrors()) {
                return null;
            } else {
                return new Result(code.generateCSharp(), code.generateStringPack());
            }
        } catch (Exception ex) {
            err.catastrophe(ex);
            return null;
        }
    }

    private MimeCodeGenerator(@NotNull MimeErrorCollector err, @NotNull VirtualFile virtualFile, boolean isDryRun) {
        this.err = err;
        this.virtualFile = virtualFile;
        className = virtualFile.getNameWithoutExtension();
        usingDeclarations = new ArrayList<>();
        sceneIds = new ArrayList<>();
        locStrings = new ArrayList<>();
        p = isDryRun ? new NullIndentPrinter() : new IndentPrinter();
        charRefs = new HashMap<>();
        charRefs.put("PL", new CharRef("PL", "ctx.player", false, true));
        mode = ClassMode.STATIC;
        namespace = "burningmime.unity.scenes";
    }

    //==================================================================================================================
    // HEADER
    //==================================================================================================================

    @Override
    public void visitFile(@NotNull PsiFile file) {
        for(PsiElement child : file.getChildren())
            child.accept(this);
    }

    @Override
    public void visitHeader(@NotNull MimeHeader o) {
        for(MimeStmt s : o.getStmtList()) {
            try {
                s.accept(this);
            } catch(Exception ex) {
                err.error(ex.getClass().getName() + "ICE while generating code for " + o.getClass().getName() + " (" + ex.getMessage() + ")\n" + ex, s);
            }
        }
    }

    @Override
    public void visitCharStmt(@NotNull MimeCharStmt o) {
        String slug = o.getCharLiteral().getText();
        boolean enableDialogue = o.getCharModifierList().isEmpty();
        if(charRefs.containsKey(slug)) {
            err.error("Already contains character with slug " + slug, o);
        } else {
            if(o.getCharSource() instanceof MimeSelfCharSource) {
                charRefs.put(slug, new CharRef(slug, "ScriptUtils.unproxyTransform(gameObject)", false, enableDialogue));
            } else {
                MimeReferenceCharSource rcs = (MimeReferenceCharSource) o.getCharSource();
                String part1 = rcs.getIdLiteralList().get(0).getText();
                String part2 = rcs.getIdLiteralList().get(1).getText();
                if(part1.equals("t")) {
                    charRefs.put(slug, new CharRef(slug, "ctx.findTarget(\"" + part2 + "\")", false, enableDialogue));
                } else if(part1.equals("f")) {
                    if(mode == ClassMode.STATIC) {
                        err.error("Fields are not allowed in static mode", o);
                    }
                    charRefs.put(slug, new CharRef(slug, "f_" + part2, true, enableDialogue));
                } else {
                    err.error("Expected MimeCharStmt to start with 'f' or 't', but was " + part1, o);
                }
            }
        }
    }

    @Override
    public void visitModeStmt(@NotNull MimeModeStmt o) {
        String text = o.getText();
        if(text.contains("interactive")) {
            mode = ClassMode.INTERACTIVE;
        } else if(text.contains("static")) {
            mode = ClassMode.STATIC;
        } else {
            err.error("Expected \"static\", \"interactive\", or \"proxy\" mode", o);
        }
    }

    @Override
    public void visitNamespaceStmt(@NotNull MimeNamespaceStmt o) {
        namespace = o.getQualifiedIdLiteral().getText();
    }

    @Override
    public void visitUsingStmt(@NotNull MimeUsingStmt o) {
        usingDeclarations.add(o.getQualifiedIdLiteral().getText().replace(" ", ""));
    }

    @Override
    public void visitSceneIdStmt(@NotNull MimeSceneIdStmt o) {
        for(MimeIdLiteral id : o.getIdLiteralList()) {
            sceneIds.add(id.getText());
        }
    }

    //==================================================================================================================
    // BASICS AND CONTROL FLOW
    //==================================================================================================================

    @Override
    public void visitBlock(@NotNull MimeBlock o) {
        for(MimeStmt s : o.getStmtList()) {
            try {
                s.accept(this);
            } catch(Exception ex) {
                err.error(ex.getClass().getName() + "ICE while generating code for " + o.getClass().getName() + " (" + ex.getMessage() + ")\n" + ex, s);
            }
        }
    }

    @Override
    public void visitCsStmt(@NotNull MimeCsStmt o) {
        p.print(escapeCSharp(o.getCSharpLiteral().getText()));
        p.println();
    }

    @Override
    public void visitBackgroundStmt(@NotNull MimeBackgroundStmt o) {
        try {
            backgroundToken = o.getBackgroundName() != null ? o.getBackgroundName().getIdLiteral().getText() : "_";
            o.getStmt().accept(this);
        } finally {
            backgroundToken = null;
        }
    }

    @Override
    public void visitPassStmt(@NotNull MimePassStmt o) {
        // pass is from python just an empty statement
    }

    @Override
    public void visitEndStmt(@NotNull MimeEndStmt o) {
        p.println("yield break;");
    }

    @Override
    public void visitGotoStmt(@NotNull MimeGotoStmt o) {
        p.println("goto " + o.getIdLiteral().getText() + ";");
    }

    @Override
    public void visitLabelStmt(@NotNull MimeLabelStmt o) {
        p.print(o.getIdLiteral().getText() + ": ");
    }

    @Override
    public void visitIfStmt(@NotNull MimeIfStmt o) {
        List<MimeBlock> blocks = o.getBlockList();
        List<MimeCSharpLiteral> conditions = o.getCSharpLiteralList();
        if(blocks.size() != conditions.size() && blocks.size() != conditions.size() + 1)
            throw new RuntimeException("blocks.size = " + blocks.size() + ", conditions.size = "
                    + conditions.size() + " (parser should not emit this!)");
        for(int i = 0; i < blocks.size(); ++i) {
            if(i < conditions.size()) {
                if(i > 0) {
                    p.print("} else ");
                }
                p.print("if(");
                p.print(escapeCSharp(conditions.get(i).getText()));
                p.println(") {");
            }
            else {
                p.println("} else {");
            }
            writeBlock(blocks.get(i));
        }
        p.closeBlock();
    }

    @Override
    public void visitWhileStmt(@NotNull MimeWhileStmt o) {
        p.print("while(");
        p.print(escapeCSharp(o.getCSharpLiteral().getText()));
        p.println(") {");
        writeBlock(o.getBlock());
        p.closeBlock();
    }

    //==================================================================================================================
    // DOMAIN-SPECIFIC
    //==================================================================================================================

    @Override
    public void visitAdjustPlayerPositionStmt(@NotNull MimeAdjustPlayerPositionStmt o) {
        p.print("yield return ScriptUtils.adjustPlayerPosition(act, tPL, ctx.findTarget(\"TARGET_" + className + "_PL\").transform)");
        finishYieldStmt();
    }

    @Override
    public void visitImageHideStmt(@NotNull MimeImageHideStmt o) {
        p.println("img.close();");
    }

    @Override
    public void visitCameraClearStmt(@NotNull MimeCameraClearStmt o) {
        p.println("ScriptUtils.clearCameraTarget();");
    }



    @Override
    public void visitImageStmt(@NotNull MimeImageStmt o) {
        hasImage = true;
        p.print("img.show(\"");
        p.print(o.getIdLiteral().getText());
        p.println("\");");
    }

    @Override
    public void visitSpawnStmt(@NotNull MimeSpawnStmt o) {
        String text = o.getText();
        boolean isSpawn = text.startsWith("spawn");
        boolean isMulti = o.getCharLiteralList().size() > 1;
        p.print("yield return ");
        if(isMulti)
            p.print("act.multi(");
        boolean isFirst = true;
        for(MimeCharLiteral c : o.getCharLiteralList()) {
            String slug = c.getText();
            if(!isFirst)
                p.print(", ");
            isFirst = false;
            if(isSpawn) {
                p.print("act.genieSpawn(t" + slug + ", tPL, ctx.findTarget(\"TARGET_" + className + "_" + slug + "\"))");
            } else {
                p.print("act.genieDespawn(t" + slug + ", tPL)");
            }
        }
        if(isMulti) {
            p.print(")");
        }
        finishYieldStmt();
    }

    @Override
    public void visitWaitStmt(@NotNull MimeWaitStmt o) {
        if(o.getNumberLiteral() != null) {
            p.print("yield return act.wait(" + o.getNumberLiteral().getText() + "f)");
        } else if(o.getIdLiteral() != null) {
            p.print("yield return act.waitFor(" + o.getIdLiteral().getText() + ")");
        } else {
            err.error("Missing number or ID literal in wait statement", o);
        }
        finishYieldStmt();
    }

    @Override
    public void visitSubStmt(@NotNull MimeSubStmt o) {
        p.print("yield return act.sub(");
        p.print(escapeCSharp(o.getCSharpLiteral().getText()));
        p.print(")");
        finishYieldStmt();
    }

    @Override
    public void visitFaceStmt(@NotNull MimeFaceStmt o) {
        String statementText = o.getText();
        boolean isTurn;
        if(statementText.startsWith("turn"))
            isTurn = true;
        else if(statementText.startsWith("look"))
            isTurn = false;
        else
        {
            err.error("Expected face statement to start with either 'look' or 'turn'", o);
            return;
        }

        for(MimeFacePart f : o.getFacePartList()) {
            boolean mutual = f.getNumberLiteral() != null;
            List<MimeCharLiteral> charLiterals = f.getCharLiteralList();
            String slug1 = charLiterals.get(0).getText();
            if(!charRefs.containsKey(slug1)) {
                err.error("Unknown char ref " + slug1, f);
                return;
            }
            String lookTarget;
            String slug2 = charLiterals.size() > 1 ? charLiterals.get(1).getText() : null;
            if(slug2 != null) {
                if(!charRefs.containsKey(slug2)) {
                    err.error("Unknown char ref " + slug2, f);
                    return;
                } else if(slug1.equals(slug2)) {
                    err.error("Character cannot face itself", f);
                    return;
                } else {
                    lookTarget = "t" + slug2;
                }
            } else {
                if(mutual) {
                    err.error("Second part of face statement is not a character, so can't do mutual turn", f);
                    mutual = false;
                }
                if(f.getIdLiteral() == null) {
                    err.error("Missing look target", f);
                    return;
                }
                lookTarget = "ctx.findTarget(\"" + f.getIdLiteral().getText() + "\")";
            }

            p.print("d");
            p.print(slug1);
            p.print(".");
            p.print(isTurn ? "turn" : "look");
            p.print("(");
            p.print(lookTarget);
            p.println(");");

            if(mutual) {
                p.print("d");
                p.print(slug2);
                p.print(".");
                p.print(isTurn ? "turn" : "look");
                p.print("(t");
                p.print(slug1);
                p.println(");");
            }
        }

        /*     private void writeFacePart(@NotNull String fromSlug, @NotNull String toSlug, boolean isTurn) {
         */
    }

    @Override
    public void visitCameraLookStmt(@NotNull MimeCameraLookStmt o) {
        String target = "transform";
        StringBuilder args = new StringBuilder();
        for(MimeCameraArg a : o.getCameraArgList()) {
            String key = a.getIdLiteralList().get(0).getText();
            switch(key) {
                case "time":
                    args.append(args.length() == 0 ? "" : ", ").append("time:").append(getFloatString(a, a.getNumberLiteral()));
                    break;
                case "rotx":
                    args.append(args.length() == 0 ? "" : ", ").append("rotX:").append(getFloatString(a, a.getNumberLiteral()));
                    break;
                case "roty":
                    args.append(args.length() == 0 ? "" : ", ").append("rotY:").append(getFloatString(a, a.getNumberLiteral()));
                    break;
                case "zoom":
                    args.append(args.length() == 0 ? "" : ", ").append("zoom:").append(getFloatString(a, a.getNumberLiteral()));
                    break;
                case "target":
                    if(a.getCharLiteral() != null) {
                        target = "t" + a.getCharLiteral().getText();
                    } else if(a.getIdLiteralList().size() > 1) {
                        target = "ctx.findTarget(\"" + a.getIdLiteralList().get(1).getText() + "\")";
                    } else if(a.getSelfLiteral() != null) {
                        target = "transform";
                    } else {
                        err.error("Invalid target specifier (expected character or ID literal)", a);
                    }
                    break;
                default:
                    if(validCameraModifiersCache == null) {
                        validCameraModifiersCache =
                                Stream.of("time", "rotx", "roty", "zoom", "target")
                                        .sorted().collect(Collectors.joining(","));
                    }
                    err.error("Unknown camera modifier " + key + " (valid modifiers=[" + validCameraModifiersCache + "])", a);
                    break;
            }
        }

        p.print("yield return act.adjustCamera(");
        p.print(target);
        if(args.length() > 0) {
            p.print(", ");
        }
        p.print(args.toString());
        p.print(")");
        finishYieldStmt();
    }

    @Override
    public void visitSetIdleStmt(@NotNull MimeSetIdleStmt o) {
        for(String slug : enumerateTargetSlugs(o.getSetIdleTargets())) {
            p.print("ScriptUtils.setConversationListen(d");
            p.print(slug);
            p.println(");");
        }
    }

    @Override
    public void visitDialogueStmt(@NotNull MimeDialogueStmt o) {
        String slug = o.getCharLiteral().getText();
        CharRef charRef = charRefs.get(slug);
        if(charRef == null) {
            err.error("Invalid character reference " + slug, o);
            return;
        }
        List<MimeDialogueSegment> segments = o.getDialogueString().getDialogueSegmentList();
        int len = segments.size();
        String text;
        if(len == 0) {
            err.error("Expected at least one dialogue segment", o);
            return;
        } else if(len == 1) {
            // optimization for common case
            text = segments.get(0).getText().trim();
        } else {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < len; ++i) {
                if(i != 0) {
                    sb.append(' ');
                }
                sb.append(segments.get(i).getText().trim());
            }
            text = sb.toString();
        }


        String initialAnimation = null, variant = null;
        StringBuilder flagsBuilder = null;
        for(MimeDialogueModifier mod : o.getDialogueString().getDialogueModifierList()) {
            String key = mod.getDialogueModifierKey().getText().trim();
            String value = mod.getDialogueModifierValue().getText().trim();
            switch (key) {
                case "var":
                    variant = value;
                    break;
                case "psource":
                    if(value.equals("SCREEN")) {
                        flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                        flagsBuilder.append("positionSource=MessageFlags.PositionSource.SCREEN");
                    } else {
                        err.error("Only psource=SCREEN is currently supported as a dialogue modifier", mod);
                    }
                    break;
                case "rx":
                    flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                    flagsBuilder.append("relativeX=MessageFlags.RelativeX.").append(value);
                    break;
                case "ry":
                    flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                    flagsBuilder.append("relativeY=MessageFlags.RelativeY.").append(value);
                    break;
                case "ry_d":
                    p.println("d" + slug + ".defaultRelativeY = MessageFlags.RelativeY." + value + ";");
                    break;
                case "tail":
                    flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                    flagsBuilder.append("tailDirection=MessageFlags.TailDirection.").append(value);
                    break;
                case "width":
                    flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                    flagsBuilder.append("width=MessageFlags.MessageWidth.").append(value);
                    break;
                case "speed":
                    flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                    flagsBuilder.append("speed=MessageFlags.MessageSpeed.").append(value);
                    break;
                case "name":
                    if(value.equals("null")) {
                        flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                        flagsBuilder.append("nameMode=MessageFlags.SpeakerNameMode.NO_NAME");
                    } else {
                        err.error("Only name=null is currently supported as a dialogue modifier", mod);
                    }
                    break;
                case "paging":
                    flagsBuilder = flagsBuilder == null ? new StringBuilder() : flagsBuilder.append(", ");
                    flagsBuilder.append("pagingMode=MessageFlags.PagingMode.").append(value);
                    break;
                case "anim":
                    initialAnimation = value;
                    if(!initialAnimation.startsWith("\"") && !initialAnimation.equals("null")) {
                        initialAnimation = "\"" + initialAnimation + "\"";
                    }
                    break;
                default:
                    if(validDialogueModifiersCache == null) {
                        validDialogueModifiersCache =
                            Stream.of("var", "psource", "rx", "ry", "ry_d", "tail", "width", "speed", "name", "paging", "ianim")
                            .sorted().collect(Collectors.joining(","));
                    }
                    err.error("Unknown dialogue modifier " + key + " (valid modifiers=[" + validDialogueModifiersCache + "])", mod);
                    break;
            }
        }

        p.print("yield return d");
        p.print(slug);
        p.print(".say(new MessageSpec{text=");
        writeStringReference(getNextLocKey(slug), text, o);
        if(initialAnimation != null) {
            p.print(", initialAnimation=");
            p.print(initialAnimation);
        }
        if(flagsBuilder != null) {
            p.print(", flags={");
            p.print(flagsBuilder.toString());
            p.print("}");
        }
        if(variant != null) {
            p.print(", variant=\"");
            p.print(variant);
            p.print("\"");
        }
        p.print("})");
        finishYieldStmt();
    }

    @Override
    public void visitWalkStmt(@NotNull MimeWalkStmt o) {
        p.print("yield return act.navigate(t");
        p.print(o.getCharLiteral().getText());
        p.print(", ctx.findTarget(\"");
        p.print(o.getIdLiteral().getText());
        p.print("\"))");
        finishYieldStmt();
    }

    @Override
    public void visitChoiceStmt(@NotNull MimeChoiceStmt o) {
        p.print("yield return dPL.ask(new[] { ");
        boolean isFirst = true;
        for(MimeChoiceString s : o.getChoiceStringList()) {
            if(!isFirst) p.print(", ");
            isFirst = false;
            writeStringReference(getNextLocKey("PL"), s.getDialogueSegment().getText(), s);
        }
        p.println(" });");
        List<MimeBlock> blocks = o.getBlockList();
        int n = blocks.size();
        for(int i = 0; i < n; ++i) {
            if(i > 0) {
                p.print("} else ");
            }
            p.println("if(dPL.choice == " + i + ") {");
            writeBlock(blocks.get(i));
        }
        // adding this exception so that if we assign a variable in each branch we don't need to assign it a default
        // this theoretically should never be hit since anything that would trigger it to return -1 will also halt the
        // underlying drama, so this is just for the C# compiler to know that.
        p.println("} else { throw new Exception(\"Unexpected choice ID \" + dPL.choice); }");
    }

    //==================================================================================================================
    // UTILITY METHODS
    //==================================================================================================================

    private void writeBlock(@NotNull MimeBlock b) {
        p.increment();
        try {
            b.accept(this);
        } finally {
            p.decrement();
        }
    }

    private void writeStringReference(@NotNull String key, @NotNull String text, @SuppressWarnings("unused") @Nullable PsiElement context) {
        locStrings.add(new LocaleStringEntry(key, text.trim()));
        p.print("strings.format(\"" + key + "\")");
    }

    private static @NotNull String escapeCSharp(@NotNull String s) {
        StringBuilder sb = new StringBuilder();
        MimeCSharpInjectionUtils.replaceTemplateParts(s.trim(), sb, null);
        return sb.toString();
    }

    private @NotNull String buildCharRefInitializers() {
        StringBuilder sb = new StringBuilder();
        for(CharRef c : iterate(charRefsSorted())) {
            sb.append("Transform t").append(c.slug).append(" = ").append(c.getTransform).append(";\n");
            if(c.generateDialogue) {
                sb.append("ctx.recordAnimationStates(t").append(c.slug).append(");\n");
                sb.append("IDialogue d").append(c.slug).append(" = act.dialogueBox(t").append(c.slug).append(");\n");
            }
        }
        return sb.toString();
    }

    private @NotNull String[] getCharRefFields() {
        return charRefsSorted().filter(cr -> cr.isField).map(cr -> cr.getTransform).toArray(String[]::new);
    }

    private @NotNull Stream<CharRef> charRefsSorted() {
        return charRefs.values().stream().sorted(Comparator.comparing(c -> c.slug));
    }

    private @NotNull String getFloatString(@NotNull PsiElement context, @Nullable MimeNumberLiteral value) {
        if(value == null) {
            err.error("Expected valid number argument", context);
            return "";
        }
        return value.getText() + "f";
    }

    private void finishYieldStmt() {
        if(backgroundToken != null) {
            p.print(".background(out BackgroundActivityToken ");
            p.print(backgroundToken);
            p.print(")");
        }
        p.println(";");
    }

    private static <T> @NotNull Iterable<T> iterate(@NotNull Stream<T> stream) {
        return stream::iterator;
    }

    private @NotNull String locPackageId() {
        return namespace + "." + className + ".g";
    }

    private @NotNull String getSourceFilePathForAttribute() {
        String path = virtualFile.getPath();
        int assetsIndex = path.indexOf("Assets/");
        if(assetsIndex < 0) {
            return path;
        } else {
            return path.substring(assetsIndex);
        }
    }

    private @Nullable String getStringPackDevPath() {
        String path = virtualFile.getPath();
        int assetsIndex = path.indexOf("Assets");
        if(assetsIndex < 0 || !path.toLowerCase().endsWith(".mime")) {
            return null;
        }
        return path.substring(assetsIndex, path.length() - 5) + ".g.en-us.stringpack";
    }



    private @NotNull String getNextLocKey(@NotNull String slug) {
        return String.format("%s%03d", slug, nextLocKeyNum++);
    }

    private @NotNull Iterable<String> enumerateTargetSlugs(MimeSetIdleTargets targets) {
        if(targets.getAllLiteral() != null) {
            return charRefs.keySet();
        } else {
            return iterate(targets.getCharLiteralList().stream().map(PsiElement::getText).distinct());
        }
    }

    //==================================================================================================================
    // BOILERPLATE/WRAPPERS
    //==================================================================================================================

    private @NotNull String generateCSharp() {
        IndentPrinter b = new IndentPrinter();
        b.println("// WARNING: This file is automatically generated. Any changes made will be lost.");
        b.println("// ReSharper disable UnusedVariable, RedundantUsingDirective, PartialTypeWithSinglePart");
        b.println("using System;");
        b.println("using System.Collections.Generic;");
        b.println("using burningmime.unity.data;");
        b.println("using burningmime.unity.dude;");
        b.println("using burningmime.unity.gameplay;");
        b.println("using burningmime.unity.ui;");
        b.println("using burningmime.unity.graphics;");
        b.println("using UnityEngine;");
        for(String using : usingDeclarations) {
            b.println("using " + using + ";");
        }
        b.println("using UDebug = UnityEngine.Debug;");
        b.println("using UObject = UnityEngine.Object;");
        b.println("using URandom = UnityEngine.Random;");
        b.println("using UAssert = UnityEngine.Assertions.Assert;");
        b.emptyLine();
        b.println("namespace " + namespace);
        b.println("{");
        b.increment();
        b.print("[MimeScriptGenerated(generatedBy=\"MimeScript Plugin for JetBrains Rider\", generatedByVersion=\"0.1-SNAPSHOT\", sourceFile=\"");
        b.print(getSourceFilePathForAttribute());
        b.println("\")]");
        if(!sceneIds.isEmpty()) {
            b.print("[RelevantScenes(");
            boolean isFirst = true;
            for(String sceneId : sceneIds) {
                if(!isFirst)
                    b.print(", ");
                isFirst = false;
                b.print("\"");
                b.print(sceneId);
                b.print("\"");
            }
            b.println(")]");
        }
        b.print("partial class ");
        b.print(className);
        if(mode == ClassMode.INTERACTIVE) {
            b.println(" : Interactive");
            b.println("{");
            b.increment();
            b.println("protected override IEnumerator<IActivity> main(IScriptContext ctx, IActivityFactory act)");
        } else {
            if(mode != ClassMode.STATIC) {
                err.error("Did not find a valid mode statement, defaulting to static", null);
            }
            b.println();
            b.println("{");
            b.increment();
            b.println("public static IEnumerator<IActivity> main(IScriptContext ctx, IActivityFactory act)");
        }
        b.println("{");
        b.increment();
        b.println("IAdventure vars = Inject.get<IAdventure>();");
        if(!locStrings.isEmpty()) {
            b.print("IStringProvider strings = Inject.get<ILocalizationService>().getStringProvider(\"");
            b.print(locPackageId());
            String stringPackDevPath = getStringPackDevPath();
            if(stringPackDevPath != null) {
                b.print("\", \"");
                b.print(stringPackDevPath);
            }
            b.println("\");");
            b.println("ctx.addDisposable(strings);");
            b.println("IStringProvider.Locals locals = new(strings);");
        }
        if(hasImage) {
            // note we want this before allocating any dialogue boxes
            b.println("IMessageImage img = ctx.messageImage();");
        }
        b.print(buildCharRefInitializers());
        b.println(p.toString().trim());
        b.decrement();
        b.println("}");
        String[] fields = getCharRefFields();
        if(fields.length > 0) {
            b.emptyLine();
            for(String f : fields)
                b.println("[SerializeField] private Transform " + f + ";");
        }
        b.decrement();
        b.println("}");
        b.decrement();
        b.println("}");
        b.emptyLine();
        return b.toString().replace("\r", "");
    }

    private @Nullable String generateStringPack() {
        return locStrings.isEmpty() ? null : MimeYamlHelper.toYaml(new StringPackWrapper(locPackageId(), "en-us",
                locStrings.stream().collect(Collectors.toMap(ls -> ls.key, ls -> ls.value))));
    }

    //==================================================================================================================
    // INNER TYPES
    //==================================================================================================================

    private enum ClassMode {
        STATIC,
        INTERACTIVE
    }

    private static final class CharRef {
        public final @NotNull String slug;
        public final @NotNull String getTransform;
        public final boolean isField;
        public final boolean generateDialogue;

        public CharRef(@NotNull String slug, @NotNull String getTransform, boolean isField, boolean generateDialogue) {
            this.slug = slug;
            this.getTransform = getTransform;
            this.isField = isField;
            this.generateDialogue = generateDialogue;
        }
    }

    private static final class LocaleStringEntry {
        public final @NotNull String key;
        public final @NotNull String value;
        public LocaleStringEntry(@NotNull String key, @NotNull String value) {
            this.key = key;
            this.value = value;
        }
    }

    private static class StringPackWrapper {
        public final @NotNull String packageId;
        public final @NotNull String locale;
        public final @NotNull Map<String, String> strings;
        public StringPackWrapper(@NotNull String packageId, @NotNull String locale, @NotNull Map<String, String> strings) {
            this.packageId = packageId;
            this.locale = locale;
            this.strings = strings;
        }
    }

    private interface IIndentPrinter {
        void increment();
        void decrement();
        void println();
        void println(String s);
        void print(String s);
        void closeBlock();
    }

    private static class NullIndentPrinter implements IIndentPrinter {
        @Override public void increment() { }
        @Override public void decrement() { }
        @Override public void println() { }
        @Override public void println(String s) { }
        @Override public void print(String s) { }
        @Override public void closeBlock() { }
    }

    private static class IndentPrinter implements IIndentPrinter {
        private static final String INDENT_STR = "    ";
        private final StringBuilder sb = new StringBuilder();
        private int indentLevel;
        private boolean pendingNewline;

        @Override public void increment() { ++indentLevel; }
        @Override public void decrement() { if(indentLevel > 0) --indentLevel; }
        @Override public String toString() { return sb.toString(); }
        @Override public void println() { pendingNewline = true; }
        @Override public void println(String s) { print(s); println(); }
        @Override public void closeBlock() { if(pendingNewline) { sb.append(" }"); } else { println(" }"); } }

        @Override public void print(String s) {
            int p = 0, i = 0;
            for(; i < s.length(); ++i) {
                char c = s.charAt(i);
                if(c == '\n') {
                    printSegment(s, p, i);
                    p = i + 1;
                    println();
                }
            }
            printSegment(s, p, i);
        }

        public void emptyLine() {
            sb.append('\n');
            pendingNewline = true;
        }

        private void printSegment(String str, int start, int end) {
            if(end - start > 0) {
                printPendingNewline();
                sb.append(str, start, end);
            }
        }

        private void printPendingNewline() {
            if(pendingNewline) {
                pendingNewline = false;
                sb.append('\n');
                //noinspection StringRepeatCanBeUsed
                for(int i = 0; i < indentLevel; ++i) {
                    sb.append(INDENT_STR);
                }
            }
        }
    }
}

