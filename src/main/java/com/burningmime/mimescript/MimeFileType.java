package com.burningmime.mimescript;

import com.burningmime.mimescript.psi.Tok;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.editor.ex.util.LayerDescriptor;
import com.intellij.openapi.editor.ex.util.LayeredLexerEditorHighlighter;
import com.intellij.openapi.editor.highlighter.EditorHighlighter;
import com.intellij.openapi.fileTypes.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.vfs.VirtualFile;
import com.jetbrains.rider.ideaInterop.fileTypes.csharp.CSharpLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.swing.Icon;

public class MimeFileType extends LanguageFileType
{
    private static final Icon ICON = IconLoader.getIcon("/icons/mime-file-icon.png", MimeFileType.class);
    public static final MimeFileType INSTANCE = new MimeFileType();
    @Override public @NotNull String getName() { return "MimeScript"; }
    @Override public @NotNull String getDescription() { return "MimeScript"; }
    @Override public @NotNull String getDefaultExtension() { return "mime"; }
    @Override public @Nullable Icon getIcon() { return ICON; }

    protected MimeFileType()
    {
        super(MimeLanguage.INSTANCE);
        FileTypeEditorHighlighterProviders.INSTANCE.addExplicitExtension(this, (project, fileType, virtualFile, colors) -> {
            if(project == null || virtualFile == null) return null;
            return new LayeredLexerEditorHighlighter(new MimeHighlighter(), colors) {{
                LayerDescriptor cSharpDescriptor = new LayerDescriptor(SyntaxHighlighterFactory.getSyntaxHighlighter(CSharpLanguage.INSTANCE, project, virtualFile), "");
                registerLayer(Tok.T_CSHARP, cSharpDescriptor);
            }};
        });
    }
}
