package com.burningmime.mimescript;

import com.burningmime.mimescript.psi.Tok;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class MimeHighlighter extends SyntaxHighlighterBase {
    private static final TextAttributesKey[] K_EMPTY = new TextAttributesKey[0];
    private static final Map<Object, TextAttributesKey[]> _map = generateMap();


    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return MimeParserDefinition.getLexer(false);
    }

    private static Map<Object, TextAttributesKey[]> generateMap()
    {
        TextAttributesKey[] K_OPERATOR = new TextAttributesKey[]{DefaultLanguageHighlighterColors.OPERATION_SIGN};
        TextAttributesKey[] K_COMMA = new TextAttributesKey[]{DefaultLanguageHighlighterColors.COMMA};
        TextAttributesKey[] K_DOT = new TextAttributesKey[]{DefaultLanguageHighlighterColors.DOT};
        TextAttributesKey[] K_KEYWORD = new TextAttributesKey[]{DefaultLanguageHighlighterColors.KEYWORD};
        TextAttributesKey[] K_TEXT = new TextAttributesKey[]{DefaultLanguageHighlighterColors.STRING};
        TextAttributesKey[] K_SLUG = new TextAttributesKey[]{DefaultLanguageHighlighterColors.INSTANCE_FIELD};
        TextAttributesKey[] K_LABEL = new TextAttributesKey[]{DefaultLanguageHighlighterColors.LABEL};
        TextAttributesKey[] K_COMMENT = new TextAttributesKey[]{DefaultLanguageHighlighterColors.LINE_COMMENT};
        TextAttributesKey[] K_CSHARP = new TextAttributesKey[]{DefaultLanguageHighlighterColors.TEMPLATE_LANGUAGE_COLOR};
        TextAttributesKey[] K_BAD = new TextAttributesKey[]{DefaultLanguageHighlighterColors.INVALID_STRING_ESCAPE};
        TextAttributesKey[] K_NUMBER = new TextAttributesKey[]{DefaultLanguageHighlighterColors.NUMBER};
        TextAttributesKey[] K_PARENS = new TextAttributesKey[]{DefaultLanguageHighlighterColors.PARENTHESES};
        TextAttributesKey[] K_BRACES = new TextAttributesKey[]{DefaultLanguageHighlighterColors.BRACES};

        HashMap<Object, TextAttributesKey[]> map = new HashMap<>();

        // keywords
        map.put(Tok.K_ADJUSTPP, K_KEYWORD);
        map.put(Tok.K_CHAR, K_KEYWORD);
        map.put(Tok.K_CHOICE, K_KEYWORD);
        map.put(Tok.K_DESPAWN, K_KEYWORD);
        map.put(Tok.K_ELIF, K_KEYWORD);
        map.put(Tok.K_ELSE, K_KEYWORD);
        map.put(Tok.K_END, K_KEYWORD);
        map.put(Tok.K_TURN, K_KEYWORD);
        map.put(Tok.K_GOTO, K_KEYWORD);
        map.put(Tok.K_IF, K_KEYWORD);
        map.put(Tok.K_LABEL, K_KEYWORD);
        map.put(Tok.K_PASS, K_KEYWORD);
        map.put(Tok.K_SELF, K_KEYWORD);
        map.put(Tok.K_SPAWN, K_KEYWORD);
        map.put(Tok.K_USING, K_KEYWORD);
        map.put(Tok.K_WAIT, K_KEYWORD);
        map.put(Tok.K_MODE, K_KEYWORD);
        map.put(Tok.K_STATIC, K_KEYWORD);
        map.put(Tok.K_INTERACTIVE, K_KEYWORD);
        map.put(Tok.K_NAMESPACE, K_KEYWORD);
        map.put(Tok.K_NODIALOGUE, K_KEYWORD);
        map.put(Tok.K_SCENEID, K_KEYWORD);
        map.put(Tok.K_IMAGE, K_KEYWORD);
        map.put(Tok.K_IMAGEHIDE, K_KEYWORD);
        map.put(Tok.K_CLOOK, K_KEYWORD);
        map.put(Tok.K_CCLEAR, K_KEYWORD);
        map.put(Tok.K_BG, K_KEYWORD);
        map.put(Tok.K_SUB, K_KEYWORD);
        map.put(Tok.K_NULL, K_KEYWORD);
        map.put(Tok.K_ALL, K_KEYWORD);
        map.put(Tok.K_SETIDLE, K_KEYWORD);
        map.put(Tok.K_WHILE, K_KEYWORD);
        map.put(Tok.K_WALK, K_KEYWORD);

        // operators
        map.put(Tok.T_GREATER, K_OPERATOR);
        map.put(Tok.T_EQUALS, K_OPERATOR);
        map.put(Tok.T_STAR, K_OPERATOR);
        map.put(Tok.T_COMMA, K_COMMA);
        map.put(Tok.T_DOT, K_DOT);
        map.put(Tok.T_LPAREN, K_PARENS);
        map.put(Tok.T_RPAREN, K_PARENS);
        map.put(Tok.T_LANGLE, K_BRACES);
        map.put(Tok.T_RANGLE, K_BRACES);

        // special
        map.put(Tok.T_CSHARP, K_CSHARP);
        map.put(Tok.T_COMMENT, K_COMMENT);
        map.put(Tok.T_TEXT, K_TEXT);
        map.put(Tok.T_ID, K_LABEL);
        map.put(Tok.T_SLUG, K_SLUG);
        map.put(Tok.T_NUMBER, K_NUMBER);
        map.put(TokenType.BAD_CHARACTER, K_BAD);
        
        return map;
    }

    @NotNull
    @Override
    public TextAttributesKey @NotNull[] getTokenHighlights(IElementType tt) {
        return _map.getOrDefault(tt, K_EMPTY);
    }
}
