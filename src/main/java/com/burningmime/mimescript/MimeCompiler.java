package com.burningmime.mimescript;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.application.ReadAction;
import com.intellij.openapi.application.WriteAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class MimeCompiler {
    private static final String NOTIFICATION_GROUP_ID = "com.burningmime.mimescript.errors";
    private static final Logger logger = Logger.getInstance(MimeCompiler.class);

    public static void compile(@NotNull Project project, @NotNull VirtualFile virtualFile, boolean reportParseErrors) {
        MimeErrorCollector err = new MimeErrorCollector(virtualFile.getName());
        try {
            MimeCodeGenerator.Result result = generateCode(project, virtualFile, err, reportParseErrors);
            if(result != null) {
                writeCode(virtualFile, result);
            }
        } catch(Exception ex) {
            err.catastrophe(ex);
        } finally {
            if(err.hasErrors()) {
                reportErrors(virtualFile, err.getErrors());
            }
        }
    }

    private static @Nullable MimeCodeGenerator.Result generateCode(@NotNull Project project, @NotNull VirtualFile virtualFile, @NotNull MimeErrorCollector err, boolean reportParseErrors) {
        return ReadAction.compute(() -> {
            MimeFile mimeFile = getPsiFile(project, virtualFile);
            if(mimeFile != null) {
                if(hasParseErrors(mimeFile)) {
                    if(reportParseErrors) {
                        err.error("Cannot parse file; skipping compilation attempt", null);
                    }
                } else {
                    return MimeCodeGenerator.run(mimeFile, err);
                }
            }
            return null;
        });
    }

    private static @Nullable MimeFile getPsiFile(@NotNull Project project, @NotNull VirtualFile virtualFile) {
        FileDocumentManager fdm = FileDocumentManager.getInstance();
        @SuppressWarnings("UnstableApiUsage") FileViewProvider fvp = fdm.findCachedPsiInAnyProject(virtualFile);
        if(fvp != null && fvp.hasLanguage(MimeLanguage.INSTANCE)) {
            PsiFile psiFile = fvp.getPsi(MimeLanguage.INSTANCE);
            if(psiFile instanceof MimeFile) {
                return (MimeFile) psiFile;
            }
        }
        PsiFile psiFile = PsiManager.getInstance(project).findFile(virtualFile);
        if(psiFile instanceof MimeFile) {
            return (MimeFile) psiFile;
        }
        return null;
    }

    private static void writeCode(@NotNull VirtualFile inFile, @NotNull MimeCodeGenerator.Result result) {
        WriteAction.run(() -> {
            String baseName = inFile.getNameWithoutExtension();
            VirtualFile dirFile = inFile;
            do {
                dirFile = dirFile.getParent();
            } while(dirFile != null && !dirFile.isDirectory());
            if(dirFile == null) {
                throw new RuntimeException(inFile + " should have a valid parent");
            }
            writeFile(dirFile, baseName + ".cs", result.cSharp);
            if(result.stringPack != null) {
                writeFile(dirFile, baseName + ".g.en-us.stringpack", result.stringPack);
            }
        });
    }

    private static void writeFile(VirtualFile dirFile, String name, String content) {
        try {
            VirtualFile outFile = dirFile.findOrCreateChildData(null, name);
            outFile.setBinaryContent(toUtf8WithBOM(content));
        } catch(IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private static byte[] toUtf8WithBOM(String s) {
        byte[] src = s.getBytes(StandardCharsets.UTF_8);
        byte[] dst = new byte[src.length + 3];
        dst[0] = (byte) 0xEF;
        dst[1] = (byte) 0xBB;
        dst[2] = (byte) 0xBF;
        System.arraycopy(src, 0, dst, 3, src.length);
        return dst;
    }

    // I know exceptions for control flow are evil; shoot me
    private static boolean hasParseErrors(PsiFile psiFile) {
        class FoundPsiErrorException extends RuntimeException { }
        try {
            psiFile.accept(new PsiRecursiveElementVisitor() {
                @Override
                public void visitElement(@NotNull PsiElement element) {
                    if(element instanceof PsiErrorElement) {
                        throw new FoundPsiErrorException();
                    }
                    super.visitElement(element);
                }
            });
        } catch(FoundPsiErrorException ex) {
            return true;
        }
        return false;
    }

    private static void reportErrors(VirtualFile virtualFile, List<MimeError> errors) {
        try {
            String errorString = buildErrorString(virtualFile, errors);
            Notification notification = new Notification(NOTIFICATION_GROUP_ID, errorString, NotificationType.ERROR);
            notification.setTitle("Error transforming " + virtualFile.getName());
            Notifications.Bus.notify(notification);
        } catch(Exception ex) {
            logger.error(ex);
        }
    }

    private static @NotNull String buildErrorString(VirtualFile virtualFile, List<MimeError> errors) {
        FileDocumentManager fdm = FileDocumentManager.getInstance();
        Document document = fdm.getDocument(virtualFile);
        OffsetToLineMap otl = document == null ? null : new OffsetToLineMap(document);
        StringWriter sw = new StringWriter();
        try(PrintWriter p = new PrintWriter(sw)) {
            boolean nextNewline = false;
            for(MimeError e : errors) {
                if(nextNewline) {
                    p.println();
                }
                if(e.context != null && otl != null) {
                    p.print("[line " + otl.getLineNumber(e.context.getTextOffset()) + "] ");
                }
                p.println(e.message);
                nextNewline = e.message.contains("\n");
            }
        }
        return sw.toString();
    }

    private static class OffsetToLineMap {
        private final int[] offsets;

        public OffsetToLineMap(Document document) {
            offsets = ReadAction.compute(() -> {
                int[] result = new int[document.getLineCount()];
                for(int i = 0; i < result.length; ++i) {
                    result[i] = document.getLineStartOffset(i);
                }
                return result;
            });
        }

        public int getLineNumber(int offset) {
            int lineno = Arrays.binarySearch(offsets, offset);
            return lineno < 0 ? -lineno : lineno + 1;
        }
    }
}


